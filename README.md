Usage
-----

    vet [-u] [-h] [-p «prefix»] [-f «format»] [«path»]

Extract a version string from the annotated tags of a Git repository.

*   `-u`

    Print usage to the standard output and exit.

    ----------------------------------------------------------------------------
    *   Required: false
    ----------------------------------------------------------------------------

*   `-h`

    Print help to the standard output and exit.

    ----------------------------------------------------------------------------
    *   Required: false
    ----------------------------------------------------------------------------

*   `-p «prefix»`

    Extract a version string only from annotated tags with the prefix
    `«prefix»`.

    ----------------------------------------------------------------------------
    *   Required: false
    *   Default: `` (the empty string)
    ----------------------------------------------------------------------------

*   `-f «format»`

    Extract a version string with the format `«format»`.

    ----------------------------------------------------------------------------
    *   Required: false
    *   Options: `semver`, `docker`
    *   Default: `semver`
    ----------------------------------------------------------------------------

*   `«path»`

    Extract a version string from the annotated tags of a Git repostory at the
    path `«path»`.

    ----------------------------------------------------------------------------
    *   Required: false
    *   Default: `.` (the current working directory)
    ----------------------------------------------------------------------------

Examples
--------

*   `vet` was designed for semantic versioning, where an annotated tag
    indicates the major and minor version of the tagged commit, and each
    subsequent commit increments the patch version.  For example,

        vet -p "version/"

    returns

        1.2.4+e509187

    for the Git commit tree

        [968a7d4]
            │
            │
            │
        [8cfec70] version/1.2
            │
            │
            │
        [8cc07f3]
            │
            │
            │
        [a25709b]
            │
            │
            │
        [c1f8626]
            │
            │
            │
        [e509187] HEAD

    That is,

    *   commit `8cfec70` has semantic version `1.2.0`,

    *   commit `8cc07f3` has semantic version `1.2.1`,

    *   commit `a25709b` has semantic version `1.2.2`,

    *   commit `c1f8626` has semantic version `1.2.3` and

    *   commit `e509187` has semantic version `1.2.4`.

*   `vet` versions pre-releases by assigning patch version `0` and
    appending an incrementing patch version to the release suffix.  For
    example,

        vet -p "version/"

    returns

        1.2.0-alpha.4+e509187

    for the Git commit tree

        [968a7d4]
            │
            │
            │
        [8cfec70] version/1.2-alpha
            │
            │
            │
        [8cc07f3]
            │
            │
            │
        [a25709b]
            │
            │
            │
        [c1f8626]
            │
            │
            │
        [e509187] HEAD

*   `vet` considers annotated tags only in the *first-parent branch* of
    the current `HEAD` commit.  For example,

        vet -p "version/"

    returns

        1.2.4+e509187

    not

        2.0.2+e509187

    for the Git commit tree

        [968a7d4]────────────────────┐
            │                        │
            │                        │
            │                        │
        [8cfec70] version/1.2    [4afc619]
            │                        │
            │                        │
            │                        │
        [8cc07f3]                [9451e8f]
            │                        │
            │                        │
            │                        │
        [a25709b]                [123d458] version/2.0
            │                        │
            │                        │
            │                        │
        [c1f8626]────────────────────┘
            │
            │
            │
        [e509187] HEAD

*   `vet` considers only the *most recent* annotated tag in the
    first-parent branch of the current `HEAD` commit.  For example,

        vet -p "version/"

    returns

        1.3.2+e509187

    not

        1.2.4+e509187

    for the Git commit tree

        [968a7d4]
            │
            │
            │
        [8cfec70] version/1.2
            │
            │
            │
        [8cc07f3]
            │
            │
            │
        [a25709b] version/1.3
            │
            │
            │
        [c1f8626]
            │
            │
            │
        [e509187] HEAD

*   `vet` considers only the most recent annotated tag *with the
    specified prefix* in the first-parent branch of the current `HEAD`
    commit.  For example,

        vet -p "version/"

     returns

        1.2.4+e509187

    not

        1.3.2+e509187

    for the Git commit tree

        [968a7d4]
            │
            │
            │
        [8cfec70] version/1.2
            │
            │
            │
        [8cc07f3]
            │
            │
            │
        [a25709b] release/1.3
            │
            │
            │
        [c1f8626]
            │
            │
            │
        [e509187] HEAD
        
